package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedicationService;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MedicationControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicationService medicationService;

    @Test
    public void insertMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationDTO medicationDTO = new MedicationDTO("Paracetamol", "None", "600 mg");

        mockMvc.perform(post("/medications")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UUID id = UUID.randomUUID();
        MedicationDTO medicationDTO = new MedicationDTO(id, "Aerius", "None", "600 mg");

        mockMvc.perform(post("/medications")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());

        mockMvc.perform(delete("/medications?id=" + id.toString()))
                .andExpect(status().isOk());

    }

    @Test
    public void updateMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UUID id = UUID.randomUUID();
        MedicationDTO medicationDTO = new MedicationDTO(id, "Algocalmni", "None", "600 mg");

        mockMvc.perform(post("/medications")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());

        medicationDTO.setName("Algocalmin");
        mockMvc.perform(put("/medications")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void readMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UUID id = UUID.randomUUID();
        MedicationDTO medicationDTO = new MedicationDTO(id, "Aerius", "None", "600 mg");

        mockMvc.perform(post("/medications")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/medications?id=" + id.toString()))
                .andExpect(status().isOk());
    }
}
