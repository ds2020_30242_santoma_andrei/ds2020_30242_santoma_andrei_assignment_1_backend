package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.DoctorDetailsDTO;
import ro.tuc.ds2020.services.DoctorService;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class DoctorControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DoctorService doctorService;

    @Test
    public void insertDoctorTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        DoctorDetailsDTO doctorDetailsDTO = new DoctorDetailsDTO("John Doe", "jdoe", "password");

        mockMvc.perform(post("/doctors")
                .content(objectMapper.writeValueAsString(doctorDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteDoctorTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UUID id = UUID.randomUUID();
        DoctorDetailsDTO doctorDetailsDTO = new DoctorDetailsDTO(id,"John Doe", "jdoe", "password");

        mockMvc.perform(post("/doctors")
                .content(objectMapper.writeValueAsString(doctorDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());

        mockMvc.perform(delete("/doctors?id=" + id.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void updateDoctorTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        DoctorDetailsDTO doctorDetailsDTO = new DoctorDetailsDTO("John Deo", "jdoe", "password");

        mockMvc.perform(post("/doctors")
                .content(objectMapper.writeValueAsString(doctorDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());

        doctorDetailsDTO.setName("John Doe");
        mockMvc.perform(put("/doctors")
                .content(objectMapper.writeValueAsString(doctorDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void readDoctorTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UUID id = UUID.randomUUID();
        DoctorDetailsDTO doctorDetailsDTO = new DoctorDetailsDTO(id,"John Doe", "jdoe", "password");

        mockMvc.perform(post("/doctors")
                .content(objectMapper.writeValueAsString(doctorDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/doctors?id=" + id.toString()))
                .andExpect(status().isOk());
    }
}
