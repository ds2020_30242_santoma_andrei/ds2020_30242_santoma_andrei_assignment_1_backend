package ro.tuc.ds2020.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DoctorDetailsDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepository doctorRepository;
    private final UserRepository userRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository, UserRepository userRepository) { this.doctorRepository = doctorRepository;
        this.userRepository = userRepository;
    }

    public List<DoctorDetailsDTO> findDoctors() {
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorBuilder::toDoctorDetailsDTO)
                .collect(Collectors.toList());
    }

    public DoctorDetailsDTO findDoctorById(UUID id) {
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        if (!doctorOptional.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }
        return DoctorBuilder.toDoctorDetailsDTO(doctorOptional.get());
    }

    public UUID insert(DoctorDetailsDTO doctorDetailsDTO) {
        User user = new User(doctorDetailsDTO.getUsername(), doctorDetailsDTO.getPassword(), "doctor");
        user = userRepository.save(user);
        Doctor doctor = DoctorBuilder.toEntity(doctorDetailsDTO);
        doctor = doctorRepository.save(doctor);
        LOGGER.debug("Doctor with id {} was inserted in db", doctor.getDoctor_id());
        return doctor.getDoctor_id();
    }

    public Boolean deleteDoctorById(UUID id) {
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        if (!doctorOptional.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        User user = userRepository.findByUsername(doctorOptional.get().getUsername()).get(0);
        userRepository.delete(user);
        doctorRepository.delete(doctorOptional.get());
        return true;
    }

    public Boolean updateDoctor(DoctorDetailsDTO doctorDetailsDTO) {

        UUID id = doctorDetailsDTO.getDoctor_id();

        Optional<Doctor> doctorOptional = doctorRepository.findById(doctorDetailsDTO.getDoctor_id());
        if (!doctorOptional.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }

        Doctor doctor = doctorOptional.get();
        List<User> users = userRepository.findByUsername(doctor.getUsername());
        if (users.size() != 1) {
            return false;
        }
        User user = users.get(0);
        user.setUsername(doctorDetailsDTO.getUsername());
        user.setPassword(doctorDetailsDTO.getPassword());
        user = userRepository.save(user);

        doctor.setName(doctorDetailsDTO.getName());
        doctor.setUsername(doctorDetailsDTO.getUsername());
        doctor.setPassword(doctorDetailsDTO.getPassword());
        doctor = doctorRepository.save(doctor);
        return true;
    }
}
