package ro.tuc.ds2020.services;

import ro.tuc.ds2020.dtos.MedicationIntakeDTO;
import ro.tuc.ds2020.dtos.MedicationTakenDTO;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface PillDispenserService {
    public String sayHelloWithHessian(String msg);

    public String findPatient(UUID uuid);

    public List<MedicationIntakeDTO> getMedicationIntakes(UUID patient_id);

    public void medicationTaken(UUID patientId, String medicationName, Boolean taken);

}
