package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Autowired
    public PatientService(
            PatientRepository patientRepository,
            CaregiverRepository caregiverRepository,
            UserRepository userRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public List<PatientDetailsDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(p -> new PatientDetailsDTO(
                        p.getPatient_id(),
                        p.getName(),
                        p.getUsername(),
                        p.getPassword(),
                        p.getBirthdate(),
                        p.getGender(),
                        p.getAddress(),
                        p.getCaregiver().getCaregiver_id(),
                        p.getMedicalRecord(),
                        p.getMedicationPlans(),
                        p.getCaregiver().getName()
                ))
                .collect(Collectors.toList());
    }

    public PatientDetailsDTO findPatientById(UUID id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        PatientDetailsDTO dto = PatientBuilder.toPatientDetailsDTO(patientOptional.get());
        dto.setCaregiver_name(patientOptional.get().getCaregiver().getName());
        return dto;
    }

    public UUID insert(PatientDetailsDTO patientDetailsDTO) {
        UUID caregiverId = patientDetailsDTO.getCaregiver_id();
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(caregiverId);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverId);
        }
        User user = new User(patientDetailsDTO.getUsername(), patientDetailsDTO.getPassword(), "patient");
        userRepository.save(user);
        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        patient.setCaregiver(caregiverOptional.get());
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getPatient_id());
        return patient.getPatient_id();
    }

    public Boolean deletePatientById(UUID id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        User user = userRepository.findByUsername(patientOptional.get().getUsername()).get(0);
        userRepository.delete(user);
        patientRepository.delete(patientOptional.get());
        return true;
    }

    public Boolean updatePatient(PatientDetailsDTO patientDetailsDTO) {
        UUID id = patientDetailsDTO.getPatient_id();

        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }

        Patient patient = patientOptional.get();

        List<User> users = userRepository.findByUsername(patient.getUsername());
        if (users.size() != 1) {
            return false;
        }
        User user = users.get(0);
        user.setUsername(patientDetailsDTO.getUsername());
        user.setPassword(patientDetailsDTO.getPassword());
        userRepository.save(user);


        patient.setName(patientDetailsDTO.getName());
        patient.setUsername(patientDetailsDTO.getUsername());
        patient.setPassword(patientDetailsDTO.getPassword());
        patient.setAddress(patientDetailsDTO.getAddress());
        patientRepository.save(patient);
        return true;
    }
}
