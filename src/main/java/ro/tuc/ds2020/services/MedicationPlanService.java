package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ParameterValidationException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository, PatientRepository patientRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
        this.patientRepository = patientRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlanList = medicationPlanRepository.findAll();
        return medicationPlanList.stream()
                .map(x -> new MedicationPlanDTO(
                        x.getMedicationPlan_id(),
                        x.getIntakeIntervals(),
                        x.getTreatmentPeriod(),
                        x.getMedications().stream()
                                .map(m -> m.getMedication_id())
                                .collect(Collectors.toList()),
                        x.getPatient().getPatient_id()
                ))
                .collect(Collectors.toList());
    }

    public List<MedicationPlanDTO> findMedicationPlansByUsername(String username) {
        List<Patient> patientList = patientRepository.findByUsername(username);
        if (patientList.size() != 1) {
            throw new ParameterValidationException("Patient username not found", null);
        }

        Set<MedicationPlan> medicationPlanList = patientList.get(0).getMedicationPlans();
        return medicationPlanList.stream()
                .map(x -> new MedicationPlanDTO(
                        x.getMedicationPlan_id(),
                        x.getIntakeIntervals(),
                        x.getTreatmentPeriod(),
                        x.getMedications().stream()
                                .map(m -> m.getMedication_id())
                                .collect(Collectors.toList()),
                        x.getPatient().getPatient_id()
                ))
                .collect(Collectors.toList());

    }

    public MedicationPlanDTO findMedicationPlanById(UUID id) {
        Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(id);
        if (!medicationPlanOptional.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id " + id);
        }
        MedicationPlan mp = medicationPlanOptional.get();
        return new MedicationPlanDTO(
                mp.getMedicationPlan_id(),
                mp.getIntakeIntervals(),
                mp.getTreatmentPeriod(),
                mp.getMedications().stream()
                        .map(m -> m.getMedication_id())
                        .collect(Collectors.toList()),
                mp.getPatient().getPatient_id()
                );
    }

    public UUID insert(MedicationPlanDTO medicationPlanDTO) {

        if (medicationPlanDTO.getMedicationIdList().size() == 0) {
            throw new ParameterValidationException("Medication plan medications id list is empty", null);
        }

        List<Optional<Medication>> medicationOptionals = medicationPlanDTO.getMedicationIdList().stream()
                .map(mid -> medicationRepository.findById(mid))
                .collect(Collectors.toList());
        for (Optional<Medication> medicationOptional : medicationOptionals) {
            if (!medicationOptional.isPresent()) {
                LOGGER.error("Medication was not found in db");
            }
        }

        Set<Medication> medicationSet;

        try {
            medicationSet = medicationOptionals.stream()
                    .map(mo -> mo.get())
                    .collect(Collectors.toSet());
        }
        catch (java.util.NoSuchElementException e) {
            throw new ParameterValidationException("Medications in medication list were not found in db", null);
        }

        Optional<Patient> patientOptional = patientRepository.findById(medicationPlanDTO.getPatient_id());
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", medicationPlanDTO.getPatient_id());
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + medicationPlanDTO.getPatient_id());
        }
        Patient p = patientOptional.get();

        MedicationPlan mp = new MedicationPlan(
                medicationPlanDTO.getIntakeIntervals(),
                medicationPlanDTO.getTreatmentPeriod(),
                medicationSet,
                p
        );

        mp = medicationPlanRepository.save(mp);

        LOGGER.debug("Medication plan with id {} was inserted in db", mp.getMedicationPlan_id());
        return mp.getMedicationPlan_id();
    }

    public Boolean deleteMedicationPlanById(UUID id) {
        Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(id);
        if (!medicationPlanOptional.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id " + id);
        }
        medicationPlanRepository.delete(medicationPlanOptional.get());
        return true;
    }

}
