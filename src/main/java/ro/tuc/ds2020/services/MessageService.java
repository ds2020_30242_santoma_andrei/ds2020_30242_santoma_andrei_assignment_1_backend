package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.SimpleMessageDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class MessageService {
    @Autowired
    private SimpMessagingTemplate websocket;

    @Autowired
    private PatientService patientService;

    @Autowired
    private CaregiverService caregiverService;

    public void sendMessage(String message) {
        System.out.println("Sending " + message);
        SimpleMessageDTO simpleMessageDTO = new SimpleMessageDTO(message);
        websocket.convertAndSend("/topic/activities", simpleMessageDTO);
    }

    public void sendMessage(ActivityDTO activityDTO){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date start = new Date();
        Date end = new Date();
        long diff = 0;
        try {
            start = sdf.parse(activityDTO.getStart());
            end = sdf.parse(activityDTO.getEnd());
            diff = end.getTime() - start.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diffSeconds = diff / 1000;

        String activity = activityDTO.getActivity();
        String anomaly = "";

        if (activity.equals("Sleeping") && diffSeconds > 7 * 60 * 60) {
            anomaly = "sleeping";
        }
        else if (activity.equals("Leaving") && diffSeconds > 5 * 60 * 60) {
            anomaly = "leaving";
        }
        else if (activity.equals("Toileting") && diffSeconds > 30 * 60) {
            anomaly = "toileting";
        }

        if (!anomaly.equals("")) {
            PatientDetailsDTO patientDetailsDTO = patientService.findPatientById(activityDTO.getPatient_id());
            CaregiverDetailsDTO caregiverDetailsDTO = caregiverService.findCaregiverById(patientDetailsDTO.getCaregiver_id());
            activityDTO.setCaregiver_username(caregiverDetailsDTO.getUsername());
            activityDTO.setPatient_name(patientDetailsDTO.getName());
            System.out.println("Sending " + activityDTO);
            websocket.convertAndSend("/topic/activities", activityDTO);
        }

    }
}
