package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public List<CaregiverDetailsDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDetailsDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDetailsDTO findCaregiverById(UUID id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDetailsDTO(caregiverOptional.get());
    }

    public UUID insert(CaregiverDetailsDTO caregiverDetailsDTO) {
        User user = new User(caregiverDetailsDTO.getUsername(), caregiverDetailsDTO.getPassword(), "caregiver");
        user = userRepository.save(user);
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDetailsDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getCaregiver_id());
        return caregiver.getCaregiver_id();
    }

    public Boolean deleteCaregiverById(UUID id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        User user = userRepository.findByUsername(caregiverOptional.get().getUsername()).get(0);
        userRepository.delete(user);
        caregiverRepository.delete(caregiverOptional.get());
        return true;
    }

    public Boolean updateCaregiver(CaregiverDetailsDTO caregiverDetailsDTO) {

        UUID id = caregiverDetailsDTO.getCaregiver_id();

        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }

        Caregiver caregiver = caregiverOptional.get();

        List<User> users = userRepository.findByUsername(caregiver.getUsername());
        if (users.size() != 1) {
            return false;
        }
        User user = users.get(0);
        user.setUsername(caregiverDetailsDTO.getUsername());
        user.setPassword(caregiverDetailsDTO.getPassword());
        user = userRepository.save(user);


        caregiver.setName(caregiverDetailsDTO.getName());
        caregiver.setUsername(caregiverDetailsDTO.getUsername());
        caregiver.setPassword(caregiverDetailsDTO.getPassword());
        caregiver.setAddress(caregiverDetailsDTO.getAddress());
        caregiver = caregiverRepository.save(caregiver);
        return true;
    }



}
