package ro.tuc.ds2020.services;


import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationIntakeDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PillDispenserServiceImpl implements PillDispenserService {

    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;
    private final MedicationService medicationService;
    private final MessageService messageService;

    public PillDispenserServiceImpl(PatientService patientService, MedicationPlanService medicationPlanService, MedicationService medicationService, MessageService messageService) {
        this.patientService = patientService;
        this.medicationPlanService = medicationPlanService;
        this.medicationService = medicationService;
        this.messageService = messageService;
    }


    public String sayHelloWithHessian(String msg) {
        System.out.println("=============server side==============");
        System.out.println("msg : " + msg);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<PatientDetailsDTO> dtos = patientService.findPatients();

        // return "Hello " + msg + " Response time :: " + new Date();
        return "Test " + dtos.get(0).getName();
    }

    public String findPatient(UUID uuid) {
        try{
            PatientDetailsDTO patientDTO = patientService.findPatientById(uuid);
            return "Patient " + patientDTO.getName() + " found";
        }
        catch (ResourceNotFoundException ex) {
            return "Patient not found";
        }
    }

    public List<MedicationIntakeDTO> getMedicationIntakes(UUID patient_id) {
        try {
            PatientDetailsDTO patientDTO = patientService.findPatientById(patient_id);
            List<MedicationPlanDTO> medicationPlanDTOs = medicationPlanService.findMedicationPlansByUsername(patientDTO.getUsername());
            List<MedicationIntakeDTO> medicationIntakeDTOs = new ArrayList<MedicationIntakeDTO>();
            for (MedicationPlanDTO medicationPlanDTO : medicationPlanDTOs) {
                String[] intakeIntervals = medicationPlanDTO.getIntakeIntervals().split(",");
                assert(intakeIntervals.length == medicationPlanDTO.getMedicationIdList().size());
                for (int i = 0; i < intakeIntervals.length; i++) {

                    UUID medicationId = medicationPlanDTO.getMedicationIdList().get(i);

                    medicationIntakeDTOs.add(
                            new MedicationIntakeDTO(
                                    intakeIntervals[i],
                                    medicationService.findMedicationById(medicationId).getName(),
                                    medicationId
                            )
                    );
                }

            }
            return medicationIntakeDTOs;
        }
        catch (ResourceNotFoundException ex) {
            return null;
        }
    }

    public void medicationTaken(UUID patientId, String medicationName, Boolean taken) {
        PatientDetailsDTO patientDTO = patientService.findPatientById(patientId);
        String message = patientDTO.getName() + (taken ? " took " : " didn't take ") + medicationName + " in the correct interval";
        messageService.sendMessage(message);
    }


}
