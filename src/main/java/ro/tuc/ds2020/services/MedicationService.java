package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository, MedicationPlanRepository medicationPlanRepository) {
        this.medicationRepository = medicationRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<MedicationDTO> findMedications() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(x -> new MedicationDTO(x.getMedication_id(), x.getName(), x.getSideEffects(), x.getDosage()))
                .collect(Collectors.toList());
    }

    public List<MedicationDTO> findMedicationsByMPId(UUID id) {
        Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(id);
        if (!medicationPlanOptional.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id " + id);
        }
        MedicationPlan mp = medicationPlanOptional.get();
        return mp.getMedications().stream()
                .map(m -> new MedicationDTO(
                        m.getName(),
                        m.getSideEffects(),
                        m.getDosage()
                ))
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(UUID id) {
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id " + id);
        }
        Medication m = medicationOptional.get();
        return new MedicationDTO(m.getName(), m.getSideEffects(), m.getDosage());
    }

    public UUID insert(MedicationDTO medicationDTO) {
        Medication m = new Medication(medicationDTO.getName(), medicationDTO.getSideEffects(), medicationDTO.getDosage());
        m = medicationRepository.save(m);
        LOGGER.debug("Medication with id {} was inserted in db", m.getMedication_id());
        return m.getMedication_id();
    }

    public Boolean deleteMedicationById(UUID id) {
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id " + id);
        }
        medicationRepository.delete(medicationOptional.get());
        return true;
    }

    public Boolean update(MedicationDTO medicationDTO) {
        UUID id = medicationDTO.getMedication_id();
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id " + id);
        }
        Medication m = medicationOptional.get();

        m.setName(medicationDTO.getName());
        m.setDosage(medicationDTO.getDosage());
        m.setSideEffects(medicationDTO.getSideEffects());
        m = medicationRepository.save(m);
        return true;

    }

}
