package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Medication;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> {

    private class MedicationDetailsDTO extends RepresentationModel<MedicationDetailsDTO> {
        @NotNull
        private String intakeInterval;

        @NotNull
        private UUID medicationId;

        public MedicationDetailsDTO(@NotNull String intakeInterval, @NotNull UUID medicationId) {
            this.intakeInterval = intakeInterval;
            this.medicationId = medicationId;
        }
    }

    private UUID medication_plan_id;

    @NotNull
    private String intakeIntervals;

    @NotNull
    private String treatmentPeriod;

    @NotNull
    private List<UUID> medicationIdList;

    @NotNull
    private UUID patient_id;

    public MedicationPlanDTO() {

    }

    public MedicationPlanDTO(@NotNull String intakeIntervals, @NotNull String treatmentPeriod, @NotNull List<UUID> medicationIdList, @NotNull UUID patient_id) {
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationIdList = medicationIdList;
        this.patient_id = patient_id;
    }

    public MedicationPlanDTO(UUID medication_plan_id, @NotNull String intakeIntervals, @NotNull String treatmentPeriod, @NotNull List<UUID> medicationIdList, @NotNull UUID patient_id) {
        this.medication_plan_id = medication_plan_id;
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationIdList = medicationIdList;
        this.patient_id = patient_id;
    }

    public UUID getMedication_plan_id() {
        return medication_plan_id;
    }

    public void setMedication_plan_id(UUID medication_plan_id) {
        this.medication_plan_id = medication_plan_id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public List<UUID> getMedicationIdList() {
        return medicationIdList;
    }

    public void setMedicationIdList(List<UUID> medicationIdList) {
        this.medicationIdList = medicationIdList;
    }

    public UUID getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationPlanDTO that = (MedicationPlanDTO) o;
        return intakeIntervals.equals(that.intakeIntervals) &&
                treatmentPeriod.equals(that.treatmentPeriod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), intakeIntervals, treatmentPeriod);
    }
}
