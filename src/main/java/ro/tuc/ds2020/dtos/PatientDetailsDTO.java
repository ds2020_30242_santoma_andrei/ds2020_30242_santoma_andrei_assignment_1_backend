package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPlan;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class PatientDetailsDTO extends RepresentationModel<PatientDetailsDTO> {

    private UUID patient_id;

    @NotNull
    private String name;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String birthdate;

    @NotNull
    private String gender;

    @NotNull
    private String address;

    @NotNull
    private UUID caregiver_id;

    private String caregiver_name;

    @NotNull
    private String medicalRecord;

    private Set<MedicationPlan> medicationPlans;

    public PatientDetailsDTO() {}

    public PatientDetailsDTO(UUID patient_id, @NotNull String name, @NotNull String username, @NotNull String password, @NotNull String birthdate, @NotNull String gender, @NotNull String address, UUID caregiver_id, @NotNull String medicalRecord, Set<MedicationPlan> medicationPlans) {
        this.patient_id = patient_id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.caregiver_id = caregiver_id;
        this.medicalRecord = medicalRecord;
        this.medicationPlans = medicationPlans;
    }

    public PatientDetailsDTO(UUID patient_id, @NotNull String name, @NotNull String username, @NotNull String password, @NotNull String birthdate, @NotNull String gender, @NotNull String address, UUID caregiver_id, @NotNull String medicalRecord, Set<MedicationPlan> medicationPlans, String caregiver_name) {
        this.patient_id = patient_id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.caregiver_id = caregiver_id;
        this.medicalRecord = medicalRecord;
        this.medicationPlans = medicationPlans;
        this.caregiver_name = caregiver_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDetailsDTO that = (PatientDetailsDTO) o;
        return name.equals(that.name) &&
                username.equals(that.username) &&
                password.equals(that.password) &&
                birthdate.equals(that.birthdate) &&
                gender.equals(that.gender) &&
                address.equals(that.address) &&
                caregiver_id.equals(that.caregiver_id) &&
                medicalRecord.equals(that.medicalRecord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, username, password, birthdate, gender, address, caregiver_id, medicalRecord);
    }

    public UUID getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UUID getCaregiver_id() {
        return caregiver_id;
    }

    public void setCaregiver_id(UUID caregiver_id) {
        this.caregiver_id = caregiver_id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getCaregiver_name() {
        return caregiver_name;
    }

    public void setCaregiver_name(String caregiver_name) {
        this.caregiver_name = caregiver_name;
    }
}
