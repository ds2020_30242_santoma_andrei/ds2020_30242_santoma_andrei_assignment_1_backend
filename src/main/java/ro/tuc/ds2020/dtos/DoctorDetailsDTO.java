package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Patient;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class DoctorDetailsDTO extends RepresentationModel<DoctorDetailsDTO> {


    private UUID doctor_id;

    @NotNull
    private String name;

    @NotNull
    private String username;

    @NotNull
    private String password;


    public DoctorDetailsDTO() { }

    public DoctorDetailsDTO(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public DoctorDetailsDTO(UUID doctor_id, String name, String username, String password) {
        this.doctor_id = doctor_id;
        this.name = name;
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DoctorDetailsDTO that = (DoctorDetailsDTO) o;
        return name.equals(that.name) &&
                username.equals(that.username) &&
                password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, username, password);
    }

    public UUID getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(UUID doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
