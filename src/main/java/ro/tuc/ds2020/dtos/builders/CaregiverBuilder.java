package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {}

    public static CaregiverDetailsDTO toCaregiverDetailsDTO(Caregiver caregiver) {
        return new CaregiverDetailsDTO(
                caregiver.getCaregiver_id(),
                caregiver.getName(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getAddress()
        );
    }

    public static Caregiver toEntity(CaregiverDetailsDTO caregiverDetailsDTO) {
        return new Caregiver(
                caregiverDetailsDTO.getName(),
                caregiverDetailsDTO.getUsername(),
                caregiverDetailsDTO.getPassword(),
                caregiverDetailsDTO.getBirthdate(),
                caregiverDetailsDTO.getGender(),
                caregiverDetailsDTO.getAddress()
        );
    }

}
