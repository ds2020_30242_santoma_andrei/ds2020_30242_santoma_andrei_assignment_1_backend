package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {}

    public static PatientDetailsDTO toPatientDetailsDTO(Patient patient) {
        return new PatientDetailsDTO(
                patient.getPatient_id(),
                patient.getName(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getCaregiver().getCaregiver_id(),
                patient.getMedicalRecord(),
                patient.getMedicationPlans()
        );
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Patient(
                patientDetailsDTO.getName(),
                patientDetailsDTO.getUsername(),
                patientDetailsDTO.getPassword(),
                patientDetailsDTO.getBirthdate(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getAddress(),
                patientDetailsDTO.getMedicalRecord()
        );
    }
}
