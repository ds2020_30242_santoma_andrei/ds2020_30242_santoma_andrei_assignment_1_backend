package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DoctorDetailsDTO;
import ro.tuc.ds2020.entities.Doctor;

public class DoctorBuilder {

    private DoctorBuilder() {

    }

    public static DoctorDetailsDTO toDoctorDetailsDTO(Doctor doctor) {
        return new DoctorDetailsDTO(
                doctor.getDoctor_id(),
                doctor.getName(),
                doctor.getUsername(),
                doctor.getPassword()
        );
    }

    public static Doctor toEntity(DoctorDetailsDTO doctorDetailsDTO) {
        return new Doctor(doctorDetailsDTO.getName(),
                doctorDetailsDTO.getUsername(),
                doctorDetailsDTO.getPassword()
        );
    }

}
