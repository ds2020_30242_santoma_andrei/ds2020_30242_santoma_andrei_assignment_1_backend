package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class LoginResponseDTO extends RepresentationModel<LoginResponseDTO> {

    @NotNull
    private String username;

    @NotNull
    private String role;

    public LoginResponseDTO(){

    }

    public LoginResponseDTO(@NotNull String username, @NotNull String role) {
        this.username = username;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LoginResponseDTO that = (LoginResponseDTO) o;
        return username.equals(that.username) &&
                role.equals(that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), username, role);
    }
}
