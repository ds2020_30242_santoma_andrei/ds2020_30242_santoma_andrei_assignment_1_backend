package ro.tuc.ds2020.dtos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.UUID;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = ActivityDTO.class)
public class ActivityDTO {

    private UUID patient_id;
    private String caregiver_username;
    private String patient_name;
    private String start;
    private String end;
    private String activity;

    public ActivityDTO() {}

    public ActivityDTO(UUID patient_id, String caregiver_username, String start, String end, String activity) {
        this.patient_id = patient_id;
        this.caregiver_username = caregiver_username;
        this.start = start;
        this.end = end;
        this.activity = activity;
    }

    public UUID getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getCaregiver_username() {
        return caregiver_username;
    }

    public void setCaregiver_username(String caregiver_username) {
        this.caregiver_username = caregiver_username;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }
}
