package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Patient;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class CaregiverDetailsDTO extends RepresentationModel<CaregiverDetailsDTO> {

    private UUID caregiver_id;

    @NotNull
    private String name;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String birthdate;

    @NotNull
    private String gender;

    @NotNull
    private String address;

    private List<PatientDetailsDTO> patientDetailsDTOs;
    
    public CaregiverDetailsDTO() { }

    public CaregiverDetailsDTO(String name, String username, String password, String birthdate, String gender, String address) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public CaregiverDetailsDTO(String name, String username, String password, String birthdate, String gender, String address, List<Patient> patients) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patientDetailsDTOs = patients.stream().map(PatientBuilder::toPatientDetailsDTO).collect(Collectors.toList());
    }

    public CaregiverDetailsDTO(UUID caregiver_id, String name, String username, String password, String birthdate, String gender, String address) {
        this.caregiver_id = caregiver_id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public CaregiverDetailsDTO(UUID caregiver_id, String name, String username, String password, String birthdate, String gender, String address, List<Patient> patients) {
        this.caregiver_id = caregiver_id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patientDetailsDTOs = patients.stream().map(PatientBuilder::toPatientDetailsDTO).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CaregiverDetailsDTO that = (CaregiverDetailsDTO) o;
        return name.equals(that.name) &&
                username.equals(that.username) &&
                password.equals(that.password) &&
                birthdate.equals(that.birthdate) &&
                gender.equals(that.gender) &&
                address.equals(that.address);
    }

    @Override
    public int hashCode() {
            return Objects.hash(super.hashCode(), name, username, password, birthdate, gender, address);
    }

    public UUID getCaregiver_id() {
        return caregiver_id;
    }

    public void setCaregiver_id(UUID caregiver_id) {
        this.caregiver_id = caregiver_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientDetailsDTO> getPatientDetailsDTOs() {
        return patientDetailsDTOs;
    }

    public void setPatientDetailsDTOs(List<PatientDetailsDTO> patientDetailsDTOs) {
        this.patientDetailsDTOs = patientDetailsDTOs;
    }
}