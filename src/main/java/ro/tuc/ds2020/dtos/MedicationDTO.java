package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Medication;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class MedicationDTO extends RepresentationModel<MedicationDTO> {

    private UUID medication_id;

    @NotNull
    private String name;

    @NotNull
    private String sideEffects;

    @NotNull
    private String dosage;

    public MedicationDTO() {}

    public MedicationDTO(UUID medication_id, @NotNull String name, @NotNull String sideEffects, @NotNull String dosage) {
        this.medication_id = medication_id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDTO(@NotNull String name, @NotNull String sideEffects, @NotNull String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationDTO that = (MedicationDTO) o;
        return name.equals(that.name) &&
                sideEffects.equals(that.sideEffects) &&
                dosage.equals(that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, sideEffects, dosage);
    }

    public UUID getMedication_id() {
        return medication_id;
    }

    public void setMedication_id(UUID medication_id) {
        this.medication_id = medication_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
