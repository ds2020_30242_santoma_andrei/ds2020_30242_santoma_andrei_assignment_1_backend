package ro.tuc.ds2020.dtos.validators;

import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.validators.annotation.RoleName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class RoleValidator implements ConstraintValidator<RoleName, String>{

    private String[] roles;

    @Override
    public void initialize(RoleName constraintAnnotation) {
        this.roles = constraintAnnotation.roles();
    }

    @Override
    public boolean isValid(String inputRole, ConstraintValidatorContext constraintValidatorContext) {
        for (String role : roles)
        {
            if (inputRole.compareTo(role) == 0)
            {
                return true;
            }
        }

        return false;
    }
}
