package ro.tuc.ds2020.dtos;

import java.io.Serializable;
import java.util.UUID;

public class MedicationIntakeDTO implements Serializable {

    private String intakeInterval;
    private String medicationName;
    private UUID medicationId;

    public MedicationIntakeDTO(String intakeInterval, String medicationName, UUID medicationId) {
        this.intakeInterval = intakeInterval;
        this.medicationName = medicationName;
        this.medicationId = medicationId;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public UUID getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(UUID medicationId) {
        this.medicationId = medicationId;
    }
}
