package ro.tuc.ds2020.dtos;

import java.io.Serializable;
import java.util.UUID;

public class MedicationTakenDTO implements Serializable {

    private UUID patientId;
    private String medicationName;
    private Boolean taken;

    public MedicationTakenDTO(UUID patientId, String medicationName, Boolean taken) {
        this.patientId = patientId;
        this.medicationName = medicationName;
        this.taken = taken;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }
}
