package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "medicationplans")
public class MedicationPlan implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID medicationPlan_id;

    @Column(nullable = false)
    private String intakeIntervals;

    @Column(nullable = false)
    private String treatmentPeriod;

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)//, nullable = false, insertable = false, updatable = false)
    @JsonBackReference
    private Patient patient;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "medication_medicationplans",
            joinColumns = @JoinColumn(name = "medicationPlan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id"))
    Set<Medication> medications;


    public MedicationPlan() {}

    public MedicationPlan(String intakeIntervals, String treatmentPeriod) {
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicationPlan(String intakeIntervals, String treatmentPeriod, Set<Medication> medications) {
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
        this.medications = medications;
    }

    public MedicationPlan(String intakeIntervals, String treatmentPeriod, Set<Medication> medications, Patient patient) {
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
        this.medications = medications;
        this.patient = patient;
    }

    public UUID getMedicationPlan_id() {
        return medicationPlan_id;
    }

    public void setMedicationPlan_id(UUID medicationPlan_id) {
        this.medicationPlan_id = medicationPlan_id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
