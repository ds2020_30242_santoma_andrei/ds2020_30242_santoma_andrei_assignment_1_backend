package ro.tuc.ds2020.controllers;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import ro.tuc.ds2020.dtos.ActivityDTO;

@Controller
public class SocketController {

    @MessageMapping("/notification")
    @SendTo("/topic/activities")
    public ActivityDTO send(@Payload ActivityDTO activityDTO) { return activityDTO; }
}