package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DoctorDetailsDTO;
import ro.tuc.ds2020.services.DoctorService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctors")

public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/getall")
    public ResponseEntity<List<DoctorDetailsDTO>> getDoctors() {
        List<DoctorDetailsDTO> dtos = doctorService.findDoctors();
        for (DoctorDetailsDTO dto : dtos) {
            Link doctorLink = linkTo(methodOn(DoctorController.class)
                    .getDoctor(dto.getDoctor_id())).withRel("doctorDetails");
            dto.add(doctorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insertDoctor(@Valid @RequestBody DoctorDetailsDTO doctorDetailsDTO) {
        UUID doctorId = doctorService.insert(doctorDetailsDTO);
        return new ResponseEntity<>(doctorId, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<DoctorDetailsDTO> getDoctor(@RequestParam("id") UUID doctorId) {
        DoctorDetailsDTO dto = doctorService.findDoctorById(doctorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deleteDoctor(@RequestParam("id") UUID id) {
        Boolean response = doctorService.deleteDoctorById(id);
        return new ResponseEntity<Boolean>(response, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Boolean> updateDoctor(@Valid @RequestBody DoctorDetailsDTO doctorDetailsDTO) {
        Boolean response = doctorService.updateDoctor(doctorDetailsDTO);
        return new ResponseEntity<Boolean>(response, HttpStatus.OK);
    }
}
