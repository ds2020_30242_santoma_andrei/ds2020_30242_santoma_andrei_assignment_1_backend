package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medications")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/getall")
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        for (MedicationDTO dto : dtos) {
            Link mLink = linkTo(methodOn(MedicationController.class)
                    .getMedication(dto.getMedication_id())).withRel("medicationDetails");
            dto.add(mLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/bympid")
    public ResponseEntity<List<MedicationDTO>> getMedicationsByMPId(@RequestParam("id") UUID id) {
        List<MedicationDTO> dtos = medicationService.findMedicationsByMPId(id);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        UUID medicationId = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Boolean> updateMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        Boolean response = medicationService.update(medicationDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deletePatient(@RequestParam("id") UUID id) {
        Boolean response = medicationService.deleteMedicationById(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<MedicationDTO> getMedication(@RequestParam("id") UUID id) {
        MedicationDTO dto = medicationService.findMedicationById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
