package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregivers")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/getall")
    public ResponseEntity<List<CaregiverDetailsDTO>> getCaregivers() {
        List<CaregiverDetailsDTO> dtos = caregiverService.findCaregivers();
        for (CaregiverDetailsDTO dto : dtos) {
            Link caregiverLink = linkTo(methodOn(CaregiverController.class)
                    .getCaregiver(dto.getCaregiver_id())).withRel("caregiverDetails");
            dto.add(caregiverLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDetailsDTO caregiverDetailsDTO) {
        UUID caregiverId = caregiverService.insert(caregiverDetailsDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<Boolean> updateCaregiver(@Valid @RequestBody CaregiverDetailsDTO caregiverDetailsDTO) {
        Boolean response = caregiverService.updateCaregiver(caregiverDetailsDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping()
    public ResponseEntity<CaregiverDetailsDTO> getCaregiver(@RequestParam("id") UUID caregiverId) {
        CaregiverDetailsDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deleteCaregiver(@RequestParam("id") UUID id) {
        Boolean response = caregiverService.deleteCaregiverById(id);
        return new ResponseEntity<Boolean>(response, HttpStatus.OK);
    }
}
