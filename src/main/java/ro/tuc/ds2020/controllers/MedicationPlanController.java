package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.services.MedicationPlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationplans")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping(value = "/getall")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlans();
        for (MedicationPlanDTO dto : dtos) {
            Link mpLink = linkTo(methodOn(MedicationPlanController.class)
            .getMedicationPlan(dto.getMedication_plan_id())).withRel("medicationPlanDetails");
        dto.add(mpLink);
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insertMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        UUID mpId = medicationPlanService.insert(medicationPlanDTO);
        return new ResponseEntity<>(mpId, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<MedicationPlanDTO> getMedicationPlan(@RequestParam("id") UUID id) {
        MedicationPlanDTO dto = medicationPlanService.findMedicationPlanById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/byusername")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlansByUsername(@RequestParam("username") String username) {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlansByUsername(username);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<Boolean> deleteMedicationPlan(@RequestParam("id") UUID id) {
        Boolean response = medicationPlanService.deleteMedicationPlanById(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
