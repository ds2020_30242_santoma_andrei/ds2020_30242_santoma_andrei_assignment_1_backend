package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/patients")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/getall")
    public ResponseEntity<List<PatientDetailsDTO>> getPatients() {
        List <PatientDetailsDTO> dtos = patientService.findPatients();
        for (PatientDetailsDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
            .getPatient(dto.getPatient_id())).withRel("patientDetails");
        dto.add(patientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDetailsDTO patientDetailsDTO) {
        UUID patientId = patientService.insert(patientDetailsDTO);
        return new ResponseEntity<>(patientId, HttpStatus.CREATED);
    }


    @GetMapping()
    public ResponseEntity<PatientDetailsDTO> getPatient(@RequestParam("id") UUID patientId) {
        PatientDetailsDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deletePatient(@RequestParam("id") UUID patientId) {
        Boolean response = patientService.deletePatientById(patientId);
        return new ResponseEntity<Boolean>(response, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Boolean> updatePatient(@Valid @RequestBody PatientDetailsDTO patientDetailsDTO) {
        Boolean response = patientService.updatePatient(patientDetailsDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
