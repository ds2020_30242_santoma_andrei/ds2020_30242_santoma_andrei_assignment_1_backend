package ro.tuc.ds2020.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.services.MessageService;

@Component
public class Consumer {

    @Autowired
    private MessageService messageService;

    @RabbitListener(queues = "${spring.rabbitmq.queue}", containerFactory = "jsaFactory")
    public void receiveMessage(ActivityDTO activityDTO) throws InterruptedException {
        System.out.println("Received " + activityDTO);
        messageService.sendMessage(activityDTO);
    }

}
