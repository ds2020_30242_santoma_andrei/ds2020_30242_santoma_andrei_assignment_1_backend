package ro.tuc.ds2020.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.services.*;


@Configuration
public class HessianConfig {

    @Autowired
    private PatientService patientService;

    @Autowired
    private MedicationPlanService medicationPlanService;

    @Autowired
    private MedicationService medicationService;

    @Autowired
    private MessageService messageService;

    @Bean(name = "/hellohessian")
    RemoteExporter sayHelloServiceHessian() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new PillDispenserServiceImpl(patientService, medicationPlanService, medicationService, messageService));
        exporter.setServiceInterface(PillDispenserService.class);
        return exporter;
    }

}